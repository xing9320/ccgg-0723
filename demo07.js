// JS prototype(continued)
// inheritance between objects.

var a = {
    x: 100
};
var b = {};
b.__proto__ = a;  // b -> b.__proto__ -> a -> a.prototype -> Object.prototype -> null
console.log(b.x);

// check of a is on the prototype chain of b
console.log(a.isPrototypeOf(b));

function Shape(){
    this.x = 0;
    this.y = 0;
}

Shape.moveStatic = function(x, y) {
    this.x = x;
    this.y = y;
}

Shape.prototype.move = function(x, y) {
    this.x = x;
    this.y = y;
}

var s = new Shape();
s.move(1, 2);

function Triangle() {
    Shape.call(this);
}

//Triangle -> Triangle.prototype -> Shape.prototype
Triangle.prototype = Object.create(Shape.prototype);
Triangle.prototype.constructor = Triangle;

var t = new Triangle();
console.log(t);
t.move(1, 1);

function Point() {}
Point.prototype.move = function(x, y){
    Shape.prototype.move.call(this, x, y);
}
var p = new Point();
p.move(1, 100);
console.log(p);


