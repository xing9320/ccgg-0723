//js basic

var assert = require("assert");
assert.ok(true);
assert.equal(1,1);

//false value; false,undefined, 0, null, '', NaN
assert.ok(!false);
assert.ok(!undefined);
assert.ok(!0);
assert.ok(!null);
assert.ok(!'');
assert.ok(!NaN);
assert.ok(!(NaN == NaN));

assert.ok(isNaN(NaN));

//Local variable and Global variable.
//nodejs: global object is "global"
//brower: "window"
//console.log(global);
function localTest() {
    var a = 1;
    b = 2;
    console.log(a);
}
localTest();

var globalTest = function() {

};
//globalTest();
console.log("global b: "+global.b);

//Self invoke function
(function(){
    console.log("Hello from IIFE!");
})();

//About functions
function avg(){
    console.log(arguments); //arguments. array-like object
    var result = 0;
    for(var i = 0; i < arguments.length; i++) {
        result += arguments[i];
    }
    return result / arguments.length;
}

console.log(avg(2, 3, 4, 5, 6, 7));

//Hoisting Features
(function(){
    hello();
    hello1;
    console.log(i);
    console.log(j);
    for (var i = 0; i < 5; i++){

    }
    console.log(i);
    var j;

    function hello(){
        console.log("hello");
    }
    var hello1 = function(){
        console.log("hello1");
    }
})();

//Different between == and ===
var s1 = "abc";//string listeral
var s2 = new String("abc"); //object
assert.ok(s1 == s2);
console.log(typeof s1);
console.log(typeof s2);
assert.ok(!(s1 === s2));// compare value and type

var x1 = {name: 'Bob'};
var x2 = {name: 'Bob'};
console.log(x1);
assert.ok(!(x1 == x2));
assert.ok(!(x1 === x2));
console.log(x1.name);
console.log(x1["name"]);
var x = {"first name": "Bob"};
console.log(x["first name"]);
